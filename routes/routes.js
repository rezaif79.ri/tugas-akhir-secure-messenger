const express = require('express')
const router = express.Router()
const controller = require("../controllers/controllers")


router.get("/", controller.getHomepage)
router.get("/login", controller.checkNotAuthenticated ,controller.getLogin)
router.get("/register", controller.checkNotAuthenticated ,controller.getRegister)
router.get("/dashboard", controller.checkAuthenticated, controller.getDashboard)
router.get("/dashboard/profile", controller.checkAuthenticated, controller.getProfile)
router.get("/dashboard/chat", controller.checkAuthenticated, controller.getRoomChat)
router.get("/reset-password", controller.checkNotAuthenticated, controller.getEmailResetForm)
router.get("/reset-password/:userId/:token", controller.checkNotAuthenticated, controller.getResetPasswordForm)

router.post("/reset-password", controller.checkNotAuthenticated, controller.postEmailResetForm)
router.post("/reset-password/:userId/:token", controller.checkNotAuthenticated, controller.postResetPasswordForm)

router.post("/dashboard/profile", controller.checkAuthenticated, controller.updateProfile)
router.post("/dashbord/messages", controller.checkAuthenticated, controller.postMessageByUsername)

router.post("/dashboard/contacts", controller.checkAuthenticated, controller.postAddContact)
router.post("/dashboard/contacts/delete/:username", controller.checkAuthenticated, controller.postDeleteContact)

// FRONT PAGE 
router.post("/login", controller.checkNotAuthenticated, controller.passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
}))

router.post("/register", controller.checkNotAuthenticated, controller.postRegister)
router.post("/logout", controller.checkAuthenticated, controller.postLogout)
module.exports =  router;
