document.onreadystatechange = function () {
    if(document.readyState === "complete"){
        var messageBody = document.querySelector('#messagesRoom');
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
        const socket = io()
        
        
        // const senderName = document.getElementById("senderMessage")
        // const recipientName = document.getElementById("recipientMessage")
        const message = document.getElementById("plainMessage")
        const url = window.location.href

        let searchParams = new URLSearchParams(url);
        
        const recipientUsername = searchParams.get("recipientUsername")
        const senderUsername = searchParams.get("username")

        const sendBtn = document.getElementById('btn-send-message')
        sendBtn.onclick = function(){
            const data = {
                senderUsername,
                recipientUsername,
                message: message.value,
                url
            }

            message.value = ""
            if(data) socket.emit("send message", data)
        }
    }
}
