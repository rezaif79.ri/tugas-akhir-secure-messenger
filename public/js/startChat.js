// Get the modal
var modal = document.getElementById("quickChatModal");
// Get the button that opens the modal
var btn = document.getElementById("quickChatBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("quick-chat-close")[0];

// When the user clicks the button, open the modal 
if(btn != null){
  btn.onclick = function() {
    modal.style.display = "block";
  }
}

// When the user clicks on <span> (x), close the modal
if(span != null){
  span.onclick = function() {
    modal.style.display = "none";
  }
}

// When the user clicks anywhere outside of the modal, close it
if(modal != null){
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
}



// Get the modal
var addContactModal = document.getElementById("addContactModal");
var addContactBtn = document.getElementById("addContactBtn");
var addContactSpan = document.getElementsByClassName("add-contact-close")[0];

addContactBtn.onclick = function() {
    addContactModal.style.display = "block";
}
addContactSpan.onclick = function() {
    addContactModal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    addContactModal.style.display = "none";
    errorModal.style.display = "none";
  }
}

console.log(window.location.hash);

// On addContact post response 
var errorSpan = document.getElementsByClassName("add-contact-close")[1];
var errorModal = document.getElementById("errorContactModal");
errorSpan.onclick = function() {
    errorModal.style.display = "none";
    window.location.hash = "";
}

if(window.location.hash == "#userNotFound"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.style.color = "#dd2020"
    errorMsg.innerHTML = "Cant add to contact! <br> Username not found"
    
}else if(window.location.hash == "#contactDuplicated"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.style.color = "#dd2020"
    errorMsg.innerHTML = "User already on your contact list!"

}else if(window.location.hash == "#addContactSuccess"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.innerHTML = "User added to your contact list!"
}else if(window.location.hash == "#errorSelfContact"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.style.color = "#dd2020"
    errorMsg.innerHTML = "Can't add yourself to your contact list!"
}else if(window.location.hash == "#contactDeleted"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.innerHTML = "Contact deleted!"
}else if(window.location.hash == "#contactNotFound"){
    errorModal.style.display = "block";
    errorMsg = document.getElementById('error-message');
    errorMsg.innerHTML = "Contact deleted!"
}else if(window.location.hash == "#profileUpdated"){
  errorModal.style.display = "block";
  errorMsg = document.getElementById('error-message');
  errorMsg.innerHTML = "Profile Updated!"
}

    