if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config()
}

const express = require('express')
const favicon = require('serve-favicon')
const path = require('path')
const morgan = require('morgan')
const helper = require('./middleware/helper')
const errorHandler = require('./controllers/errorHandler')
const ioHandler = require("./controllers/socketControllers")
const routes = require('./routes/routes')
const flash = require('express-flash')
const session = require('express-session')
const passport = require('passport')
const cors = require('cors');

const port = process.env.PORT || 8000;

let app = express()
let server = require('http').createServer(app)
let io = require('socket.io')(server)

app.set('view engine', 'ejs')
app.set('trust proxy', 1);
app.use(morgan('tiny'))
app.use(express.urlencoded({extended:false}))
app.use(cors())

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { 
        secure: false,
        maxAge:  1*60*60*1000 }
}))

app.use(flash())
app.use(passport.initialize())
app.use(passport.session())


// Set public dir to be static
app.use('/public', express.static('public'))
app.use(favicon(path.join(__dirname, 'public', 'logo.ico')))

app.use(routes)
app.use(errorHandler.notFound); // 404 Error Handler
app.use(errorHandler.serverError); // Internal server error handler

io.on("connection", ioHandler.connectedHandler);
  

server.listen(port, () => console.log(`Server running at http://localhost:${port}`))




