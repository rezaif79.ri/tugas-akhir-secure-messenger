'use strict';
const { v4: uuidv4 } = require("uuid");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('chatRooms', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: uuidv4(),
      },
      user1Id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Users",
          },
          key: "id",
          onDelete: "CASCADE"
        }
      },
      user2Id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: "Users",
            
          },
          key: "id",
          onDelete: "CASCADE"
        }
      },
      key:{
        type: Sequelize.TEXT,
        allownull:false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('chatRooms');
  }
};