'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('MessageLogs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      messageId: {
        type: Sequelize.INTEGER,
        unique: true,
        references: {
          model: {
            tableName: "Messages",
          },
          key: "id",
          onDelete: "CASCADE"
        }
      },
      messageLength: {
        type: Sequelize.INTEGER
      },
      totalTime: {
        type: Sequelize.DECIMAL(8,5)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('MessageLogs');
  }
};