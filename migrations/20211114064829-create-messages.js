'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Messages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      }, 
      chatId:{
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: "chatRooms",
          },
          key: "id",
          onDelete: "CASCADE"
        }
      },
      sender: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: {
            tableName: "Users",
          },
          key: "username",
          onDelete: "CASCADE"
        }
      },
      recipient: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: {
            tableName: "Users",
          },
          key: "username",
          onDelete: "CASCADE"
        }
      },
      cipherText: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Messages');
  }
};