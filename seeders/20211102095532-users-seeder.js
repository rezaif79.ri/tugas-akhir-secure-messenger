'use strict';
const bcrypt = require('bcrypt')
const cryptojs = require('crypto-js')


module.exports = {
  up: async (queryInterface, Sequelize) => {
    const privateKey1 = await bcrypt.genSalt(10)
    const privateKey2 = await bcrypt.genSalt(10)
    const privateKey3 = await bcrypt.genSalt(10)

    const key1 = await bcrypt.hash("PasswordReza123", privateKey1)
    const key2 = await bcrypt.hash("PasswordJordan123", privateKey2)
    const key3 = await bcrypt.hash("PasswordBilly123", privateKey3)

    const nonce1 = cryptojs.lib.WordArray.random(64/2).toString()
    const nonce2 = cryptojs.lib.WordArray.random(64/2).toString()
    const nonce3 = cryptojs.lib.WordArray.random(64/2).toString()

    const encryptPassword1 = await cryptojs.AES.encrypt(nonce1, key1.substr(-16)).toString()
    const encryptPassword2 = await cryptojs.AES.encrypt(nonce2, key2.substr(-16)).toString()
    const encryptPassword3 = await cryptojs.AES.encrypt(nonce3, key3.substr(-16)).toString()

    await queryInterface.bulkInsert('Users', [{
      username: "rezaif79",
      fullname: "Reza Izzan Fadhila",
      email: "reza@gmail.com",
      encryptPassword: encryptPassword1,
      privateKey: privateKey1,
      nonce: nonce1,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    },
    {
      username: "jordan0",
      fullname: "User Admin",
      email: "admin@gmail.com",
      encryptPassword: encryptPassword2,
      privateKey: privateKey2,
      nonce: nonce2,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    },
    {
      username: "admin",
      fullname: "SUPER ADMIN",
      email: "Super@gmail.com",
      encryptPassword: encryptPassword3,
      privateKey: privateKey3,
      nonce: nonce3,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    }
  ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
