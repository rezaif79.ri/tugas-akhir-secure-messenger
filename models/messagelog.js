'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MessageLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  MessageLog.init({
    messageId: DataTypes.INTEGER,
    messageLength: DataTypes.INTEGER,
    totalTime: DataTypes.DECIMAL(8,5)
  }, {
    sequelize,
    modelName: 'MessageLog',
  });
  return MessageLog;
};