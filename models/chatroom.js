'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class chatRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // chatRoom.hasMany(models.Messages, {
      //   foreignKey: "id",
      //   onDelete: "CASCADE"
      // })
    }
  };
  chatRoom.init({
    user1Id: DataTypes.INTEGER,
    user2Id: DataTypes.INTEGER,
    key: DataTypes.TEXT,
  }, {
    sequelize,
    modelName: 'chatRoom',
  });
  return chatRoom;
};