'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RegisterLog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  RegisterLog.init({
    userId: DataTypes.INTEGER,
    passwordLength: DataTypes.INTEGER,
    totalTime: DataTypes.DECIMAL(8,5)
  }, {
    sequelize,
    modelName: 'RegisterLog',
  });
  return RegisterLog;
};