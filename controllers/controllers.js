const cryptojs = require('crypto-js');
const sendMail = require('../utils/nodeMailer');
const { Users, Contacts, Messages, chatRoom, RegisterLog, MessageLog, MessageDecryptLog, ResetToken } = require('../models');
const bcrypt = require('bcrypt');
const {performance} = require('perf_hooks');
const { v4: uuidv4 } = require('uuid');

const passport = require('passport')
const initializePassport = require('./passport-config.js');
const { Op } = require('sequelize');
const chatroom = require('../models/chatroom');
initializePassport(passport)

// Routes
const controller = {
    getHomepage(req, res){
        return res.render('index.ejs')
    }, 
    getLogin(req, res){
        return res.render('login.ejs')
    },
    getRegister(req, res){
        return res.render('register.ejs', {message:null})
    },
    async getProfile(req, res){
        const user = await Users.findByPk(req.user.id)
        const session_user = req.user
        return res.render('profile.ejs', {user, session_user})
    },
    async updateProfile(req, res){
        const { fullname, email } = req.body
        const user = await Users.findByPk(req.user.id)
        await user.update({
            fullname, email
        })

        return res.redirect("/dashboard/profile#profileUpdated")
    },
    async getDashboard(req, res){
        const getContactList = await Contacts.findAll({
            where:{
                ownerId: req.user.id
            }
        })

        const contactList = await Promise.all(getContactList.map( async(contact) => {
            const frnd = await Users.findOne({where: {username: contact.username }})
            if(!!frnd) contact.fullname = frnd.fullname 
            return getContactList
        }))

        const recentChat = await chatRoom.findAll({
            where:{
                [Op.or]: [{user1Id:  req.user.id}, {user2Id: req.user.id}]  
            },
            order: [[ 'createdAt', 'DESC' ]]
        })

        const messages = []

        if(!!recentChat){
            for (let i = 0; i < recentChat.length; i++) {
                const c = recentChat[i];
                const message = await Messages.findOne({
                    where: { chatId: c.id },
                    order: [ [ 'createdAt', 'DESC' ]],
                })
                if(!!message){
                    // const sender = await Users.findOne({where:{username: chat.sender}, attributes:['id', 'username', 'salt']})
                    const key = bcrypt.hashSync(c.id, c.key)
                    message.cipherText = cryptojs.AES.decrypt(message.cipherText, key).toString(cryptojs.enc.Utf8)
                    message.time = new Date(message.createdAt).toLocaleString('en-US')
                    messages.push(message)
                }
            }
        }

        const session_user = req.user
        messages.sort((x, y) => y.createdAt - x.createdAt);
        return res.render('dashboard.ejs', { 
            username: req.user.username,
            contactList: contactList[0],
            activeChat: messages,
            session_user
         })
    },
    passport, //for post login
    async postRegister(req, res){
        try {
            const { username, fullname, email, password, passwordRetype } = req.body
            if(password !== passwordRetype) {
                const message = "Password do not match!"
                return res.render("register.ejs",{message})
            }

            const duplicate = await Users.findOne({
                    where:{username
                }})
                
            if(duplicate!=null){ 
                const message = "Username already taken!"
                return res.render("register.ejs",{message})
            }

            const startRegister = parseFloat(performance.now()).toFixed(4)
            
            const privateKey = bcrypt.genSaltSync(10)
            const key = bcrypt.hashSync(password, privateKey)
            const nonce = cryptojs.lib.WordArray.random(64/2).toString()
            // const salt = cryptojs.lib.WordArray.random(16/2).toString() // todo: next update remove this

            const encryptPassword = cryptojs.AES.encrypt(nonce, key.substring(-16)).toString()
            const endRegister = parseFloat(performance.now()).toFixed(4)
            const totalRegister = endRegister - startRegister

            const user = await Users.create({
                username, fullname, email, privateKey, encryptPassword, nonce
            })

            await RegisterLog.create({
                userId: user.id,
                passwordLength: password.length,
                totalTime: totalRegister
            })

            return res.redirect("/login")
        } catch (err) {
            return res.render("register.ejs",{message: err.message})
        }
        
        
        
    },
    async postAddContact(req, res){
        const username = req.body.addUsername
        if(req.user.username == username) return res.redirect("/dashboard#errorSelfContact")
        try{
            const user = await Users.findOne({
                where:{
                    username
                }
            })
    
            if(user == null){
                return res.redirect("/dashboard#userNotFound")
            }

            const duplicate = await Contacts.findOne({
                where:{
                    ownerId: req.user.id,
                    username: username
                }
            })

            if(duplicate) return res.redirect("/dashboard#contactDuplicated")

            await Contacts.create({
                ownerId: req.user.id,
                username,
                displayName: username
            })

            return res.redirect("/dashboard#addContactSuccess")
        }catch(e){
            console.log(e.message)
        }
    },
    async postDeleteContact(req, res){
        const {username} = req.params
        const ownerId = req.user.id
        const contact = await Contacts.findOne({ where: {
            ownerId,
            username
        }})
        if(contact == null) res.redirect("/dashboard#contactNotFound")
        contact.destroy()
        res.redirect("/dashboard#contactDeleted")
    },
    async getRoomChat(req,res){
        if(req.query.username == null) return res.redirect("/dashboard#usernameNull")
        if(req.query.recipientName == null) return res.redirect("/dashboard#recipientNull")
        if(req.query.username != req.user.username || req.query.recipientName == req.user.username) return res.redirect("/dashboard#usernameNull")

        const recipient = await Users.findOne({where:{username: req.query.recipientName}, attributes:['id', 'username']})
        if(!recipient) return res.redirect("/dashboard#recipientNotFOund")
        const sender = await Users.findOne({where:{username: req.user.username}, attributes:['id', 'username']})

        let chat = await chatRoom.findOne({where:{
            [Op.or]:[
                {[Op.and]: [{user1Id: req.user.id}, {user2Id: recipient.id}]},
                {[Op.and]: [{user1Id: recipient.id}, {user2Id: req.user.id}]}
        ]
        }});

        if(!chat){
            try {
                chat = await chatRoom.create({
                    id: uuidv4() ,
                    user1Id: req.user.id,
                    user2Id: recipient.id,
                    key: bcrypt.genSaltSync(10)
                })
            } catch (err) {
                console.log("================")
                console.log(err)
            }
            
        }

        const messages = await Messages.findAll({
            where:{
                chatId: chat.id    
            },
            order: [
                ['createdAt', 'DESC'],]
        })

        const messageList = []
        if(!!messages){
            
           for (let m = 0; m < messages.length; m++) {
                const element = messages[m];
                
                const startDecryptMessage = parseFloat(performance.now()).toFixed(4)

                const key = bcrypt.hashSync(chat.id, chat.key)
                element.cipherText = cryptojs.AES.decrypt(element.cipherText, key).toString(cryptojs.enc.Utf8)
                const time = new Date(element.createdAt).toLocaleString('en-US')
                element.time = time
                messageList.push(element)

                const endDecryptMessage = parseFloat(performance.now()).toFixed(4)
                const totalDecryptMessage = endDecryptMessage - startDecryptMessage

                await MessageDecryptLog.create({
                    messageId: element.id,
                    messageLength: element.cipherText.length,
                    totalTime: totalDecryptMessage
                })
           }
        }

        const session_user = req.user
        return res.render("chat", {
            recipient: recipient.username,
            username: req.user.username,
            messageList,
            session_user
        })
    },
    async postMessageByUsername(req,res){
        if(req.query.username != req.user.username) return res.redirect("/dashboard#idNull")
        const sender = await Users.findOne({where:{username: req.user.username},attributes:['id', 'username']})
        const {message, recipientUsername} = req.body

        const recipient = await Users.findOne({where:{username: recipientUsername}})
        if(recipient == null) return res.redirect("/dashboard#contactNotFound")

        const chat = await chatRoom.findOne({where:{
            [Op.or]:[
                {[Op.and]: [{user1Id: req.user.id}, {user2Id: recipient.id}]},
                {[Op.and]: [{user1Id: recipient.id}, {user2Id: req.user.id}]}
            ]
        }})
        // Todo: Change Text Encryption System

        const startPostMessage = parseFloat(performance.now()).toFixed(4)
        const key = bcrypt.hashSync(chat.id, chat.key)
        const cipherText = cryptojs.AES.encrypt(message, key).toString()

        const endPostMessage = parseFloat(performance.now()).toFixed(4)
        const totalPostMessage = endPostMessage - startPostMessage

        const send = await Messages.create({
            chatId: chat.id,
            sender: sender.username,
            recipient: recipient.username,
            cipherText
        })

        await MessageLog.create({
            messageId: send.id,
            messageLength: message.length,
            totalTime: totalPostMessage
        })

        return res.redirect(`/dashboard/chat?recipientName=${recipient.username}&username=${req.user.username}`)

    },
    postLogout(req, res){
        req.logOut()
        res.redirect('/login')
    },
    async getEmailResetForm(req, res){
        return res.render('reset');
    },
    async postEmailResetForm(req, res){
        try {
            const { email } = req.body;
            if(!email) return res.render('info', {info:{
                title: "Email not found",
                text: "Email maybe doesnt exist"
            }});

            const user = await Users.findOne({where: {email: email}});
            if(!user) return res.render('info', {info:{
                title: "User not found",
                text: "No user registered with this email"
            }});

            const token = await ResetToken.create({
                userId: user.id,
                token: cryptojs.lib.WordArray.random(64/2).toString(),
                used: false
            });

            const link = `${process.env.BASE_URL}/reset-password/${token.userId}/${token.token}`;
            await sendMail(user.email, "Reset Password - Secure Messenger APP", link)
            return res.render('info', {info:{
                title: "Password Reset",
                text: "Password Reset link has been sent to your email account"
            }});
        } catch (e) {
            console.log(e.message);
            return res.render('info', {info:{
                title: "Error encountered",
                text: "An error occured"
            }});
        }
    },
    async getResetPasswordForm(req, res){
        const { userId, token } = req.params;
        if(!userId || !token) res.redirect("/404");
        
        const resetToken = await ResetToken.findOne({where:{
            userId, token
        }});
        
        if(!resetToken || resetToken.used) return res.render('info', {info:{
            title: "Invalid reset token",
            text: "Reset token doesnt exist or already used"
        }});

        return res.render('resetPassword', {info: {
            userId: userId,
            token: token
        }})
    },
    async postResetPasswordForm(req, res){
        const { userId, token } = req.params;
        const { password } = req.body;
        if(!userId || !token) res.redirect("/404");
        
        const resetToken = await ResetToken.findOne({where:{
            userId, token
        }});
        
        if(!resetToken || resetToken.used) return res.render('info', {info:{
            title: "Invalid reset token",
            text: "Reset token doesnt exist or already used"
        }});

        const user = await Users.findOne({where: {id: userId}});

        const privateKey = bcrypt.genSaltSync(10)
        const key = bcrypt.hashSync(password, privateKey)
        const nonce = cryptojs.lib.WordArray.random(64/2).toString()
        const encryptPassword = cryptojs.AES.encrypt(nonce, key.substring(-16)).toString()

        await user.update({
            privateKey, encryptPassword, nonce
        })

        await resetToken.update({
            used: true
        })

        return res.redirect("/login")
    },
    checkAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
          return next()
        }
        res.redirect('/login')
    },
      
    checkNotAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
          return res.redirect('/dashboard')
        }
        next()
    }
}


module.exports = controller;
