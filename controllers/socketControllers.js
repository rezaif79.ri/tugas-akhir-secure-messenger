const { Messages } = require('../models');

const ioHandler = {
    connectedHandler(socket){
        console.log("User connected: " + socket.id)

        socket.on("send message", function(data){
            console.log(data)
        })

        socket.on('disconnect', () => {
            console.log('user disconnected');
        });

    }
}

module.exports = ioHandler