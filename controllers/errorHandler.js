const path = require('path');
const url = require('url');

function getFullUrl(request){
    return url.format({
        protocol: request.protocol,
        host: request.get('host'),
        pathname: request.originalUrl
    })
    
}

const errorHandler = {
    notFound(req, res, next){
        return res.render(path.resolve("views/404.ejs"))
    },
    
    serverError(err, req, res, next){
        return res.render(path.resolve("views/500.ejs"), {
            errMessage: err.message
        })
    }
}

module.exports = errorHandler;
