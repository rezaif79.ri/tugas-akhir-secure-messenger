const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt');
const cryptojs = require('crypto-js');
const { Users, LoginLog } = require('../models')
const {performance} = require('perf_hooks');

function initialize(passport){
    const authenticateUser = async (username, password, done) =>{
        const user = await Users.findOne({
            where: {
                username: username
            }
        })

        if(user == null){
            return done(null, false, {message: "Username not found!"})
        }

        try{

            const startLogin = parseFloat(performance.now()).toFixed(4)
            
            const key = bcrypt.hashSync(password, user.privateKey)
            const validation = cryptojs.AES.decrypt(user.encryptPassword, key.substring(-16)).toString(cryptojs.enc.Utf8)
            
            if(validation == user.nonce){
                const newPrivateKey = bcrypt.genSaltSync(10)
                const key = bcrypt.hashSync(password, newPrivateKey)
                const newNonce = cryptojs.lib.WordArray.random(64/2).toString()
                const newEncryptPassword = cryptojs.AES.encrypt(newNonce, key.substring(-16)).toString()
                
                const endLogin = parseFloat(performance.now()).toFixed(4)
                const totalLogin = endLogin - startLogin

                await LoginLog.create({
                    userId: user.id,
                    passwordLength: password.length,
                    totalTime: totalLogin
                })

                await user.update({
                    privateKey: newPrivateKey,
                    encryptPassword: newEncryptPassword,
                    nonce: newNonce
                })
                
                return done(null, user)
            }else{
                return done(null, false, {message: 'Password incorrect'})
            }
        }catch(e){
            console.log(e.message)
            return done(e)
        }
    }

    passport.use(new LocalStrategy({ usernameField: 'username'}, authenticateUser))
    passport.serializeUser((user, done) => done(null, user.username))
    passport.deserializeUser( async(username, done) => { 
        const user = await Users.findOne({
            where: {
                username: username
            },
            attributes: ['id', 'username', 'fullname']
        })
        return done(null, user)
    })
}

module.exports = initialize 