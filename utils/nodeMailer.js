const nodemailer = require('nodemailer');

module.exports = async(email, subject, text)=>{
    try{
        const transporter = nodemailer.createTransport({
            host: process.env.HOST,
            service: process.env.SERIVCE,
            port: 587,
            // secure: true,
            secureConnection: false, // TLS requires secureConnection to be false
            auth:{
                user: process.env.USER,
                pass: process.env.PASS
            },
            tls: {
                ciphers:'SSLv3'
            },
            debug: true,
            logger: true
        });

        await transporter.sendMail({
            from: process.env.USER,
            to: email,
            subject: subject,
            text: text
        });
        console.log("Email send succesfully");

    }catch(e){
        
        console.log(e.message)
    }
}